import sys
import logging





class arena:

  # Keeps track of the arena and what movements that give what result.
  # Change the shape of arena is being done here.
  # Easy to alter by adding directions and behavior. Like wrapping 
  # or rotating the arena.

  def __init__(self, width=5, height=5):
    self.width = width
    self.height = height

  # Returns the new location if it possible to get there with out any error
  def getNewLocation(self, movingObject, direction, speed = 1):
    newY = movingObject.currentY
    newX = movingObject.currentX
    if movingObject.headings[movingObject.currentHeading] == 'N':
      newY = movingObject.currentY - (direction * speed)  
    if movingObject.headings[movingObject.currentHeading] == 'S':
      newY = movingObject.currentY + (direction * speed)  

    if movingObject.headings[movingObject.currentHeading] == 'E':
      newX = movingObject.currentX + (direction * speed)  
    if movingObject.headings[movingObject.currentHeading] == 'W':
      newX = movingObject.currentX - (direction * speed)  

    failure = False
    if not newY in range(0, self.height):
      failure = True

    if not newX in range(0, self.width):
      failure = True

    return int(newX), int(newY), failure


class movingObject:

  def __init__(self,arena = arena, initX = 0, initY = 0 ):
    self.currentX = initX
    self.currentY = initY
    self.failed = False
    self.arena = arena

    # The possible headings in order. Other headings and their order are easy to add.
    self.headings = ['N', 'E', 'S', 'W']

    # Pointer to the current heading, defaults to north
    self.currentHeading = 0

  # Changes the object forward facing heading self.currentHeading
  def changeHeading(self, rotationHeading):
    if rotationHeading == 'cw':
      if self.currentHeading == 3:
        self.currentHeading = 0
      else:
        self.currentHeading += 1

    if rotationHeading == 'ccw':
      if self.currentHeading == 0:
        self.currentHeading = 3
      else:
        self.currentHeading -= 1

  # Moves the object if possible. Sets the new current posistion in X and Y
  def moveObject(self, moveDirection ):
    if moveDirection == 'forward':
      self.currentX, self.currentY, failed = self.arena.getNewLocation(self, 1)
      if failed == True:
        self.failed = True
        logging.debug('Out')
    if moveDirection == 'backwards':
      self.currentX, self.currentY, failed = self.arena.getNewLocation(self, -1)
      if failed == True:
        logging.debug('Out')
        self.failed = True
      pass

  # Determine success and print result
  def getResult(self):
    if self.failed == True:
      print('[-1,-1]')
      logging.info('Showing failure')

    else:
      logging.info('Showing success')
      print('[{},{}]'.format(self.currentX, self.currentY))


# Interpret the command given and throw it at the moving object in question
# This is separated for ease of reading and ease of implementing different ways to controll the simulation.
def executeCommand(movingObject, command):
  # Print the current result
  if command == 0:
    movingObject.getResult()

  # Move forward
  if command == 1:
    movingObject.moveObject('forward')

  # Move backwards
  if command == 2:
    movingObject.moveObject('backwards')

  # Rotate CW
  if command == 3:
    movingObject.changeHeading('cw')

  # Rotate CCW
  if command == 4:
    movingObject.changeHeading('ccw')


# Change to alter logging behavior
loglevel=10

logging.basicConfig(level=loglevel, filename="logfile.txt", filemode="a+", format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.info('Starting')


parameters = []
initated = False


# Command line arguments
if len(sys.argv) > 1:
  if len(sys.argv) < 5:
    print('Du behöver ange parameterar, Arena X, Arena Y, Initial position X, Initital position Y')
    sys.exit()
  else:
    parameters = sys.argv[1:5]
    a = arena(width = int(parameters[0]), height= int(parameters[1]))
    m = movingObject(initX = int(parameters[2]), initY = int(parameters[3]), arena = a)
    initated = True
    logging.debug('Objects created from parameters A:[{},{}], Position: [{},{}]'.format(m.arena.width, m.arena.height, m.currentX, m.currentY))
    
# Stdin 

for line in sys.stdin: 
  if initated == True:
    try:
      line = int(line)
    except:
      line = 9
    executeCommand(m, int(line))
    logging.debug('X: {}, Y {}, Heading {}'.format(m.currentX, m.currentY, m.headings[m.currentHeading]))
    if line == 0:
      sys.exit()
  if len(parameters) < 4:
    parameters.append(int(line))
  if len(parameters) == 4 and initated == False:
    a = arena(width = parameters[0], height= parameters[1])
    m = movingObject(initX = parameters[2], initY = parameters[3], arena = a)
    initated = True
    logging.debug('Objects created from stdin A:[{},{}], Position: [{},{}]'.format(m.arena.width, m.arena.height, m.currentX, m.currentY))
