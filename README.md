# Boxmove.py

Simulate the move of an object in an arena (box)


### Prerequisites

Python 3

### Installing

Just clone and you are done. Perhaps you need to make a virtualenv or similiar.
## Running simulation

python boxmove.py [width, height, initX, initY]

Optional parameters 
- width = width of the arena
- height = height of the arena
- initX = initial position in X
- initY = initial position in Y

All or no parameters needs to be given. 

If no parameters are given boxmove.py assumes that the first 4 integers are the parameters in the same order as stated above.

The obejct is allways heading north at the start of the simulation.

Then issue commands 0-5 (see explanation below) to run the simulation

- 0 Stop and show the resulting coordinates [-1,-1] means it went outside of the arena at some time
- 1 Move forward 1 unit
- 2 Move backwards 1 unti
- 3 Rotate 90 deg clockwise
- 4 Rotate 90 deg counter clockwise


Please note that origo is in the top left corner.

python boxmove.py < input.txt should result in [0,1]

## Documenation

The source code is prette much documented in comments and obvious coding.

## Logging

Logging is made to loggfile.txt

By altering loglevel=10 to 20 in the code you can make that less noicy

## Author

Christian Nilsson (chin.se)

## License

This project is licensed under the public domain

## Acknowledgments

* They choose to be undisclosed, on stage waering masks ;)



